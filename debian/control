Source: gobgp
Section: net
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Bernat <bernat@debian.org>
Build-Depends: debhelper (>= 9.20160709),
               bash-completion,
               dh-golang,
               golang-go,
               golang-github-burntsushi-toml-dev,
               golang-github-coreos-go-systemd-dev,
               golang-github-dgryski-go-farm-dev,
               golang-gopkg-eapache-channels.v1-dev,
               golang-goprotobuf-dev,
               golang-github-google-uuid-dev,
               golang-go-flags-dev,
               golang-github-k-sone-critbitgo-dev,
               golang-github-kr-pretty-dev,
               golang-github-sirupsen-logrus-dev (>= 0.11.0-2~),
               golang-github-spf13-cobra-dev,
               golang-github-spf13-viper-dev,
               golang-github-stretchr-testify-dev,
               golang-github-vishvananda-netlink-dev,
               golang-golang-x-net-dev,
               golang-google-grpc-dev,
               protobuf-compiler (>= 3)
Standards-Version: 4.1.2
Homepage: https://github.com/osrg/gobgp
Vcs-Browser: https://salsa.debian.org/go-team/packages/gobgp
Vcs-Git: https://salsa.debian.org/go-team/packages/gobgp.git
XS-Go-Import-Path: github.com/osrg/gobgp

Package: gobgpd
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: BGP implemented in Go (daemon and client)
 GoBGP is an open source BGP implementation designed from scratch for
 modern environment and implemented in Go. It is designed to exploit
 multicore processors and can be easily integrated with other software
 through an RPC API.
 .
 This package contains both the daemon and the client.

Package: golang-github-osrg-gobgp-dev
Architecture: all
Section: devel
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-go,
         golang-github-armon-go-radix-dev,
         golang-toml-dev,
         golang-gopkg-eapache-channels.v1-dev,
         golang-goprotobuf-dev,
         golang-go-flags-dev,
         golang-github-influxdb-influxdb-dev,
         golang-github-kr-pretty-dev,
         golang-github-satori-go.uuid-dev,
         golang-github-sirupsen-logrus-dev,
         golang-github-socketplane-libovsdb-dev,
         golang-github-spf13-cobra-dev,
         golang-github-spf13-viper-dev,
         golang-github-stretchr-testify-dev,
         golang-github-vishvananda-netlink-dev,
         golang-golang-x-net-dev,
         golang-google-grpc-dev,
         golang-gopkg-tomb.v2-dev
Description: BGP implemented in Go (source)
 GoBGP is an open source BGP implementation designed from scratch for
 modern environment and implemented in Go. It is designed to exploit
 multicore processors and can be easily integrated with other software
 through an RPC API.
 .
 This package contains the source code.
